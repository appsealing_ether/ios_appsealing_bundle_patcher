#! /bin/bash

# 로컬 테스트를 위해 입력된 library에 입력된 bundle_id의 hash를 패치해주는 스크립트

# Usage: java -jar {bundle patch jar} {sdk packages dir} {user bundle id} {output archived sdk path}

# java -jar /appsealing/resources/common/ios_bundle_patcher/ios_appsealing_bundle_patcher_for_local_test.jar /appsealing/resources/ios_sdk/sdk_tamplate com.appsealing.inka.LibTestApp

java -jar /Users/inka/Documents/project/ios_appsealing_bundle_patcher/ios_appsealing_bundle_patcher_for_local_test.jar "$1" "$2"