#! /bin/bash

# 입력된 sdk 폴더에서 appsealing libraries에 입력된 bundle id에 해당하는 sha-256 hash를 패치하고 요청된 경로에 zip으로 묶어주는 스크립트
# 원본 sdk 템플릿 폴더와 동일 경로에 "원본_tmp" 폴더에 복사본을 만들어 패치 후 삭제한다. sdk 템플릿 폴더 경로에 write 권한이 필요하다.  

# Usage: java -jar {bundle patch jar} {sdk packages dir} {user bundle id} {output archived sdk path}

# java -jar /appsealing/resources/common/ios_bundle_patcher/ios_appsealing_bundle_patcher.jar /appsealing/resources/ios_sdk/sdk_tamplate com.appsealing.inka.LibTestApp /appsealing/resources/ios_sdk/iOS_SDK_for_XXX.zip

java -jar /Users/inka/Documents/project/ios_appsealing_bundle_patcher/ios_appsealing_bundle_patcher.jar "$1" "$2" "$3"
