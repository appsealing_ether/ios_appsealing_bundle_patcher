package com.inka.appsealing.ios_appsealing_bundle_patcher;

public class BundleManager {
	
	// API로 부터 전달받은 사용자 bundle id와 해시 값 
	public String user_bundle_id;
	public String user_bundle_hash_string;
	public byte[] user_bundle_hash;
	
	// sdk에 predefine된 appsealing bundle id hash
	// com.appsealing.sdk : 3028ab7a4e43464e58abfdc83173bc6d8e7cfe003b4a13d39d55e553ff8c4a55
	// bundle patcher는 이 hash string을 사용자 bundle id의 hash로 patch한다.
	public String appsealing_sdk_bundle_hash_string = "3028ab7a4e43464e58abfdc83173bc6d8e7cfe003b4a13d39d55e553ff8c4a55";
	public byte[] appsealing_sdk_bundle_hash = { 
			(byte)0x33, (byte)0x30, (byte)0x32, (byte)0x38, (byte)0x61, (byte)0x62, (byte)0x37, (byte)0x61, (byte)0x34, (byte)0x65, (byte)0x34, (byte)0x33, (byte)0x34, (byte)0x36, (byte)0x34, (byte)0x65, 
			(byte)0x35, (byte)0x38, (byte)0x61, (byte)0x62, (byte)0x66, (byte)0x64, (byte)0x63, (byte)0x38, (byte)0x33, (byte)0x31, (byte)0x37, (byte)0x33, (byte)0x62, (byte)0x63, (byte)0x36, (byte)0x64, 
			(byte)0x38, (byte)0x65, (byte)0x37, (byte)0x63, (byte)0x66, (byte)0x65, (byte)0x30, (byte)0x30, (byte)0x33, (byte)0x62, (byte)0x34, (byte)0x61, (byte)0x31, (byte)0x33, (byte)0x64, (byte)0x33, 
			(byte)0x39, (byte)0x64, (byte)0x35, (byte)0x35, (byte)0x65, (byte)0x35, (byte)0x35, (byte)0x33, (byte)0x66, (byte)0x66, (byte)0x38, (byte)0x63, (byte)0x34, (byte)0x61, (byte)0x35, (byte)0x35
	};
	
	public BundleManager() {
		user_bundle_id = "";
		user_bundle_hash_string = "";
		user_bundle_hash = null;
	}		
		
}
