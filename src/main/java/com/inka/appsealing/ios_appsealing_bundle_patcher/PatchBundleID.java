package com.inka.appsealing.ios_appsealing_bundle_patcher;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.Vector;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.FileUtils;

public class PatchBundleID {
	public static void main(String[] args) {
		System.out.println("\n************************************************************************************************");
		System.out.println("\tAppSealing iOS BundleID Patcher");
		System.out.println("************************************************************************************************\n");

		if (args.length != 3) {
			System.out.println("\nUsage: java -jar {this.jar} {SDK_tamplate_folder_path} {user bundle id} {Patched_SDK_output_zip_path}\n");
			return;
		}

		// ------------------------------------------------------------------------------------------------------------
		// Step 1) generate user id hash (sha256)
		BundleManager bundleManager = new BundleManager();
		bundleManager.user_bundle_id = args[1];

		System.out.println("# Generate user bundle id hash (SHA-256)...");

		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.reset();
			digest.update(bundleManager.user_bundle_id.getBytes());
			String sha2Value = String.format("%04x", new BigInteger(1, digest.digest()));

			// 앞 자리가 0인 경우 무시되는 0 추가
			while (sha2Value.length() < 64) {
				sha2Value = "0" + sha2Value;
			}

			bundleManager.user_bundle_hash_string = sha2Value;
			bundleManager.user_bundle_hash = bundleManager.user_bundle_hash_string.getBytes();

			System.out.println(String.format("user bundle_id: %s", bundleManager.user_bundle_id));
			System.out.println(String.format("bundle hash: %s", sha2Value));
			System.out.println("Generate user bundle id hash complete.\n");

		} catch (Exception e) {
			e.printStackTrace();
		}

		// ------------------------------------------------------------------------------------------------------------
		// Step 2) patch user bundle id hash to static libraries
		System.out.println("\n# Patch user bundle id hash to appsealing ios libraries...");
		
		// TODO: 복사해서 작업 후 삭제 (/tmp/packages)
		// server s3에 복사할 수 있도록 권한 부여 필요.
		File sdkBaseTamplateFolder = new File(args[0]);
		File tmpSdkBaseFolder = new File(sdkBaseTamplateFolder.getParentFile(), String.format("%s_tmp", sdkBaseTamplateFolder.getName()));
				
		try {
			FileUtils.copyDirectory(sdkBaseTamplateFolder, tmpSdkBaseFolder);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		File appsealing_library = new File(tmpSdkBaseFolder,
				String.format("/Libraries/ios/%s", DEF.APPSEALING_SECURITY_ARCHIVE_NAME));
		File appsealing_library_bitcode = new File(tmpSdkBaseFolder,
				String.format("/Libraries/ios-bitcode/%s", DEF.APPSEALING_SECURITY_ARCHIVE_NAME));

		if (!tmpSdkBaseFolder.exists()) {
			System.out.println(String.format("\n[!] Cannot found SDK folder: %s\n", tmpSdkBaseFolder.getAbsolutePath()));
			System.exit(-1);
		}

		if (!appsealing_library.exists()) {
			System.out.println(
					"\n[!] Cannot found appsealing security library file: %s\n" + appsealing_library.getAbsolutePath());
			System.exit(-1);
		}

		if (!appsealing_library_bitcode.exists()) {
			System.out.println("\n[!] Cannot found appsealing security library for bitcode: %s\n"
					+ appsealing_library_bitcode.getAbsolutePath());
			System.exit(-1);
		}

		System.out.println(String.format("pre-defined bundle hash: %s (len: %d)",
				bundleManager.appsealing_sdk_bundle_hash_string, bundleManager.appsealing_sdk_bundle_hash.length));
		System.out.println(String.format("user bundle id hash: %s (len: %d)", 
				bundleManager.user_bundle_hash_string, bundleManager.user_bundle_hash.length));

		try {

			// bundle hash patch to archive
			BinaryKeyPatch_ex(appsealing_library, bundleManager.appsealing_sdk_bundle_hash,
					bundleManager.user_bundle_hash);
			BinaryKeyPatch_ex(appsealing_library_bitcode, bundleManager.appsealing_sdk_bundle_hash,
					bundleManager.user_bundle_hash);

			System.out.println("Patch user bundle id hash to appsealing libraries complete.\n");
		} catch (IOException e) {
			e.printStackTrace();
		}


		// ------------------------------------------------------------------------------------------------------------
		// Step 3) Output archiving
		System.out.println(String.format("\n# Packaging appsealing sdk for %s...", bundleManager.user_bundle_id));

		Collection<File>  sdkFileList = FileUtils.listFiles(tmpSdkBaseFolder, null, true);
		Vector<File> sdk_files = new Vector<File>(sdkFileList);
		
		int bufSize = 1024;
		byte[] buf = new byte[bufSize];

		FileInputStream fis = null;
		ZipArchiveOutputStream zos = null;
		BufferedInputStream bis = null;
		File output_archive = null;

		try {

			output_archive = new File(args[2]);
			
			String output_folder_name = "";
			int extPosition = output_archive.getName().lastIndexOf(".");
			if(extPosition == 0) {
				output_folder_name = output_archive.getName();
			} else {
				output_folder_name = output_archive.getName().substring(0, extPosition);
			}
			
			output_folder_name = output_archive.getName().substring(0, output_archive.getName().length()-4);
				
			zos = new ZipArchiveOutputStream(new BufferedOutputStream(new FileOutputStream( output_archive )));
			//System.out.println("output archive: " + output_archive.getAbsolutePath() );

			for(File file : sdk_files) { 

				zos.setEncoding("UTF-8");

				try { 
				fis = new FileInputStream(file);
				bis = new BufferedInputStream(fis, bufSize);
				
				String[] tokens = file.getAbsolutePath().split(tmpSdkBaseFolder.getAbsolutePath());
				String entryName = String.format("%s%s%s", output_folder_name, File.separator, tokens[1]);
				zos.putArchiveEntry(new ZipArchiveEntry( entryName ));
				} catch(Exception e) {
					
					e.printStackTrace();
					continue;
				}

				int len;
				while ((len = bis.read(buf, 0, bufSize)) != -1) {
					zos.write(buf, 0, len);
				}

				bis.close();
				fis.close();
				zos.closeArchiveEntry();
			}

			zos.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {

				if (zos != null) {
					zos.close();
				}

				if (fis != null) {
					fis.close();
				}

				if (bis != null) {
					bis.close();
				}
				
				// delete tmpSdkBaseFolder
				FileUtils.deleteQuietly(tmpSdkBaseFolder);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		System.out.println("Packaging appsealing sdk complete.");
		System.out.println(String.format("packages: %s\n", output_archive.getAbsolutePath()));

		System.out.println("\n************************************************************************************************");
		System.out.println("\tEnd of patch process.");
		System.out.println("************************************************************************************************\n");
	}

	public static int BinaryKeyPatch_ex(File targetFile, byte[] targetValue, byte[] replaceValue) throws IOException {
		
		System.out.println(String.format("Patching hash to '%s'", targetFile.getAbsolutePath()));
		
		if (targetValue.length != replaceValue.length) {
			throw new IOException(String.format("[!] Invalid target length, target: %d, replace: %d", targetValue.length, replaceValue.length));
		}

		byte[] FileData = Files.readAllBytes(targetFile.toPath());
		int nPatchBytes = 0;
		int idx = indexOf(FileData, targetValue, 0);
		while (idx != -1) {
			for (int i = 0; i < targetValue.length; i++) {
				FileData[idx + i] = replaceValue[i];
				nPatchBytes += 1;
			}
			idx = indexOf(FileData, targetValue, idx);
		}
		
		// targetValue는 library가 지원하는 machine에 따라 여러개 검색될 수 있다. 
		if(nPatchBytes == 0 || nPatchBytes % targetValue.length != 0) {
			System.out.println(String.format("[!] Failed hash patch. targetValue len: %d, nPatchedBytes: %d", 
					targetValue.length, nPatchBytes));
			System.exit(-1);
		}

		Files.write(targetFile.toPath(), FileData);
		
		System.out.println(String.format("Patch hash complete."));

		return nPatchBytes;
	}

	public static int indexOf(byte[] outerArray, byte[] smallerArray, int startIdx) {
		for (int i = startIdx; i < outerArray.length - smallerArray.length + 1; ++i) {
			boolean found = true;
			for (int j = 0; j < smallerArray.length; ++j) {
				if (outerArray[i + j] != smallerArray[j]) {
					found = false;
					break;
				}
			}
			if (found)
				return i;
		}
		return -1;
	}
}
